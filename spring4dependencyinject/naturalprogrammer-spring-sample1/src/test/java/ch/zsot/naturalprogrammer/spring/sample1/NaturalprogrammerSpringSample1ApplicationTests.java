package ch.zsot.naturalprogrammer.spring.sample1;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = NaturalprogrammerSpringSample1Application.class)
@WebAppConfiguration
public class NaturalprogrammerSpringSample1ApplicationTests {

	@Test
	public void contextLoads() {
	}

}
